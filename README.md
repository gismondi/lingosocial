# Lingosocial

The goal of Lingosocial is connecting people who speak the same language or help them to learn a new one by talking to native speakers.

The pdf report can be found [here](https://lingosocial.approu.it/Final_report.pdf)

## Trying the website
The website has been hosted [here](https://lingosocial.approu.it/) for convenience.

## Running the project in your computer
1. Clone the repository
2. `cd` into the folder
3. run `npm install` to automatically retrieve needed dependencies
4. start a development server by running `npm run dev`
5. instead, you can build the production version files by running `npm run build`

## License and copyright
Copyright 2020-2021 Massimo Gismondi, Tanmay Chakraborty

We decided to license our work with a **GPLv3 or later** license.

Find the complete license text at [https://www.gnu.org/licenses/gpl-3.0.txt](https://www.gnu.org/licenses/gpl-3.0.txt) or in the enclosed COPYING file.

## Other copyright notices
The main logo has been taken from Flaticon. Here's the attribution as request by the logo author https://www.flaticon.com/authors/freepik

The user picture image is attributed to its original author https://www.flaticon.com/authors/bqlqn