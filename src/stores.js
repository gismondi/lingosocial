import {writableSession} from './store-util';

export const user_data = writableSession('user_data', {});

export const fake_data = writableSession('fake_data', [
    {
        language: "English",
        author: "Yourfren",
        content: "Lorem ipsum, a really simple text!",
        liked: false
    },
    {
        language: "Italian",
        author: "Dummy user",
        content: `Apelle, figlio di Apollo fece una palla di pelle di pollo. Tutti i pesci vennero a galla, per vedere la palla di pelle di pollo fatta da Apelle, figlio di Apollo.`,
        liked: false
    },
]);

export const trending_data = writableSession('trending_data', [
    {
        language: "English",
        author: "User 13",
        content:
            "Lorem ipsum, a really simple text! But it's trending anyway!",
    },
    {
        language: "French",
        author: "User 15",
        content: "Baguette!",
    },
]);